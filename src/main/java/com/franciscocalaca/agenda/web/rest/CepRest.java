package com.franciscocalaca.agenda.web.rest;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.franciscocalaca.agenda.bo.CepBo;
import com.franciscocalaca.agenda.model.Cep;

@RestController
@RequestMapping("/cep")
public class CepRest {

	@Autowired
	private CepBo cepBo;
	
	@GetMapping("/{cep}")
	public Cep get(@PathVariable("cep") String cep) throws IOException {
		return cepBo.consultar(cep);
	}
	
}
