package com.franciscocalaca.agenda.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.franciscocalaca.agenda.dao.PessoaDao;
import com.franciscocalaca.agenda.model.Pessoa;

@RestController
@RequestMapping("/pessoa")
public class PessoaRest {

	@Autowired
	private PessoaDao pessoaDao;
	
	@PostMapping
	public void post(@RequestBody Pessoa pessoa) {
		pessoaDao.save(pessoa);
	}
	
	@GetMapping("/{id}")
	public Pessoa get(@PathVariable("id")Long id){
		return pessoaDao.findById(id).get();
	}
	
	@GetMapping
	public List<Pessoa> get(){
		return pessoaDao.findAll();
	}
	
}
