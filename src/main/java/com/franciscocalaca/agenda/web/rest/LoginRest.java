package com.franciscocalaca.agenda.web.rest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.franciscocalaca.http.utils.Token;

@RestController
@RequestMapping("/login")
public class LoginRest {

	@Autowired
	private AuthTokenService authTokenService;
	
	@PostMapping
	public Map<String, Object> post(@RequestBody Map<String, Object> request){
		String usuario = (String) request.get("usuario");
		String senha = (String) request.get("senha");
		
		
		Token token = authTokenService.getToken(usuario, senha);
		
		Map<String, Object> resposta = new HashMap<String, Object>();
		resposta.put("access_token", token.getAccessToken());
		return resposta;
	}
	
}
