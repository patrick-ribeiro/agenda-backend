package com.franciscocalaca.agenda.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.franciscocalaca.agenda.dao.ProdutoDao;
import com.franciscocalaca.agenda.model.Produto;

@RestController
@RequestMapping("/produto")
public class ProdutoRest {

	@Autowired
	private ProdutoDao produtoDao;
	
	@GetMapping
	public List<Produto> get(){
		return produtoDao.findAll();
	}
	
}
