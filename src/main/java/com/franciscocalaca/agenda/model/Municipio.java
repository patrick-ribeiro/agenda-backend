package com.franciscocalaca.agenda.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity
public class Municipio {

	@Id
	private Long id;
	
	private String nome;
	
	@Enumerated(EnumType.STRING)
	private EnumUf uf;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public EnumUf getUf() {
		return uf;
	}

	public void setUf(EnumUf uf) {
		this.uf = uf;
	}

	public Long getId() {
		return id;
	}
	
}
