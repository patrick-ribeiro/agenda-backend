package com.franciscocalaca.agenda.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.franciscocalaca.agenda.model.Municipio;

@Repository
public interface MunicipioDao extends JpaRepository<Municipio, Long>{

	@Query("select m from Municipio m where upper(m.nome) like upper(concat('%', :nome,'%')) order by m.nome")
	List<Municipio> findByNome(@Param("nome") String nome);
	
}
