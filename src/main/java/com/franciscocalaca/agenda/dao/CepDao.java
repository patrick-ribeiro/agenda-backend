package com.franciscocalaca.agenda.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.franciscocalaca.agenda.model.Cep;

@Repository
public interface CepDao extends JpaRepository<Cep, String>{

}
