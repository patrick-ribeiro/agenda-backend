package com.franciscocalaca.agenda.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.franciscocalaca.agenda.model.Produto;

/*
 * Dao = Data Access Object
 */
public interface ProdutoDao extends JpaRepository<Produto, Long>{

}
