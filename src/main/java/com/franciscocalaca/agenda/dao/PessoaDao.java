package com.franciscocalaca.agenda.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.franciscocalaca.agenda.model.Pessoa;

public interface PessoaDao extends JpaRepository<Pessoa, Long>{

}
